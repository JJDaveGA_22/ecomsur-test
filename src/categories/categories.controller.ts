import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { MongoIdPipe } from 'src/common/mongo-id.pipe';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dtos/create-category.dto';
import { UpdateCategoryDto } from './dtos/update-category.dto';

@Controller('categories')
@ApiTags('Categorías')
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}

  @Post()
  @ApiOperation({
    summary: 'Crear categoría',
  })
  create(@Body() payload: CreateCategoryDto) {
    return this.categoriesService.create(payload);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Obtener una categoría',
  })
  getOneById(@Param('id', MongoIdPipe) id: string) {
    return this.categoriesService.findOneById(id);
  }

  @Put(':id')
  @ApiOperation({
    summary: 'Actualizar categoría',
  })
  update(
    @Param('id', MongoIdPipe) id: string,
    @Body() payload: UpdateCategoryDto,
  ) {
    return this.categoriesService.update(id, payload);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Eliminar categoría',
  })
  delete(@Param('id', MongoIdPipe) id: string) {
    return this.categoriesService.remove(id);
  }
}
