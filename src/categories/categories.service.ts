import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Category } from './entities/category.entity';
import { CreateCategoryDto } from './dtos/create-category.dto';
import { UpdateCategoryDto } from './dtos/update-category.dto';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectModel(Category.name) private categoryModel: Model<Category>,
  ) {}

  create(data: CreateCategoryDto): Promise<Category> {
    const category = new this.categoryModel(data);
    return category.save();
  }

  async findOneById(id: string) {
    const product = await this.categoryModel
      .findOne({ id })
      .populate('category')
      .exec();
    if (!product) {
      throw new NotFoundException(`Category #${id} not found`);
    }
    return product;
  }

  update(id: string, changes: UpdateCategoryDto) {
    const product = this.categoryModel
      .findByIdAndUpdate(id, { $set: changes }, { new: true })
      .exec();
    if (!product) {
      throw new NotFoundException(`Category #${id} not found`);
    }
    return product;
  }

  remove(id: string) {
    return this.categoryModel.findByIdAndDelete(id);
  }
}
