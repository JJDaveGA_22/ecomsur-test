import { IsPositive, IsOptional, Min, ValidateIf } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FilterProductDto {
  @IsOptional()
  @IsPositive()
  @ApiProperty({ description: 'Limit' })
  limit: number;

  @IsOptional()
  @Min(0)
  @ApiProperty({ description: 'Offset' })
  offset: number;

  @IsOptional()
  @Min(0)
  @ApiProperty({ description: 'Precio mínimo' })
  minPrice: number;

  @ValidateIf((params) => params.minPrice)
  @IsPositive()
  @ApiProperty({ description: 'Precio máximo' })
  maxPrice: number;
}
