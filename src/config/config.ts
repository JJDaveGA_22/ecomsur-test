import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    mongoConfig: {
      host: process.env.MONGODB_HOST,
      dbName: process.env.MONGODB_DATABASE,
      user: process.env.MONGODB_USERNAME,
      password: process.env.MONGODB_PASSWORD,
      port: parseInt(process.env.MONGODB_PORT, 10),
      connectionType: process.env.MONGODB_CONNECTION_TYPE,
    },
  };
});
